<?php require("db.php"); ?>
<?php require("functions/str_replace.php"); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bücherverwaltung</title>
        <link rel="shortcut icon" href="css/favicon.png">

        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <?php require("inc/navibar.inc.php"); ?>

        <div class="container">
        <?php
            if(!$_GET['site'] || $_GET['site'] == 'overview'){require("inc/overview.php");}
            if($_GET['site'] == 'add-datas'){require("inc/add-datas.php");}
            if($_GET['site'] == 'edit-datas'){require("inc/edit-datas.php");}
            if($_GET['site'] == 'settings'){require("inc/settings.php");}
        ?>
        </div>

        <script src="js/1110-jquery.min.js"></script>
        <script src="js/191-jquery.min.js"></script>
        <script src="js/171-jquery.min.js"></script>
        <script src="js/stupidtable.js?dev"></script>
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(function(){
                // Helper function to convert a string of the form "Mar 15, 1987" into a Date object.
                var date_from_string = function(str) {
                    var months = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];
                    var pattern = "^([a-zA-Z]{3})\\s*(\\d{1,2}),\\s*(\\d{4})$";
                    var re = new RegExp(pattern);
                    var DateParts = re.exec(str).slice(1);

                    var Year = DateParts[2];
                    var Month = $.inArray(DateParts[0].toLowerCase(), months);
                    var Day = DateParts[1];

                    return new Date(Year, Month, Day);
                }

                var table = $("table").stupidtable({
                    "date": function(a,b) {
                        // Get these into date objects for comparison.
                        aDate = date_from_string(a);
                        bDate = date_from_string(b);
                        return aDate - bDate;
                    }
                });

                table.on("beforetablesort", function (event, data) {
                    // Apply a "disabled" look to the table while sorting.
                    // Using addClass for "testing" as it takes slightly longer to render.
                    $("#msg").text("Sortieren...");
                    $("table").addClass("disabled");
                });

                table.on("aftertablesort", function (event, data) {
                    // Reset loading message.
                    $("#msg").html("&nbsp;");
                    $("table").removeClass("disabled");

                    var th = $(this).find("th");
                    th.find(".arrow").remove();
                    var dir = $.fn.stupidtable.dir;

                    var arrow = data.direction === dir.ASC ? "&ensp;<i class='fa fa-caret-up'></i>" : "&ensp;<i class='fa fa-caret-down'></i>";
                    th.eq(data.column).append('<span class="arrow">' + arrow +'</span>');
                });
            });
        </script>
        <script type="text/javascript">
            function delbook(id){
                var answer = confirm("Sind Sie sich sicher das Sie dieses Buch löschen möchten?")
                var xmlhttp;
                if (answer){
                    var url='inc/del-datas.php/?id='+id;
                    if (window.XMLHttpRequest){
                        ajax=new XMLHttpRequest();
                    }else if (window.ActiveXObject){
                        ajax=new ActiveXObject("Microsoft.XMLHTTP");
                    }else{
                        alert("Ihr Browser unterstützt kein XMLHTTP!");
                    }
                    ajax.onreadystatechange=function(){
                        if(ajax.readyState==4){
                            //document.myForm.time.value=xmlhttp.responseText;
                        }
                    }
                    ajax.open("GET",url,true);
                    ajax.send(null); 
                }  
            }
        </script>
    </body>
</html>
