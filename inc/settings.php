<div class="row">
	<div class="col-md-3">
		<div class="panel panel-default">
  			<div class="panel-heading">Autoren</div>
  			<div class="panel-body">
	    		<a href="?site=settings&amp;menu=add-autor" class="btn btn-sm btn-block btn-primary">Hinzufügen</a>
				<a href="?site=settings&amp;menu=edit-autor" class="btn btn-sm btn-block btn-primary">Editieren</a>
				<a href="?site=settings&amp;menu=del-autor" class="btn btn-sm btn-block btn-danger">Löschen</a>
  			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-default">
  			<div class="panel-heading">Formate</div>
  			<div class="panel-body">
	    		<a href="?site=settings&amp;menu=add-format" class="btn btn-sm btn-block btn-primary">Hinzufügen</a>
				<a href="?site=settings&amp;menu=edit-format" class="btn btn-sm btn-block btn-primary">Editieren</a>
				<a href="?site=settings&amp;menu=del-format" class="btn btn-sm btn-block btn-danger">Löschen</a>
  			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-default">
  			<div class="panel-heading">Verlage</div>
  			<div class="panel-body">
		    	<a href="?site=settings&amp;menu=add-publisher" class="btn btn-sm btn-block btn-primary">Hinzufügen</a>
				<a href="?site=settings&amp;menu=edit-publisher" class="btn btn-sm btn-block btn-primary">Editieren</a>
				<a href="?site=settings&amp;menu=del-publisher" class="btn btn-sm btn-block btn-danger">Löschen</a>
  			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-default">
  			<div class="panel-heading">Genres</div>
  			<div class="panel-body">
		    	<a href="?site=settings&amp;menu=add-genre" class="btn btn-sm btn-block btn-primary">Hinzufügen</a>
				<a href="?site=settings&amp;menu=edit-genre" class="btn btn-sm btn-block btn-primary">Editieren</a>
				<a href="?site=settings&amp;menu=del-genre" class="btn btn-sm btn-block btn-danger">Löschen</a>
  			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="panel panel-default">
  			<div class="panel-heading">Allgemeine Einstellungen</div>
  			<div class="panel-body">
		    	<a href="?site=settings&amp;menu=view_listing" class="btn btn-sm btn-block btn-primary">Übersichtsansicht ändern</a>
  			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="panel panel-default">
  			<div class="panel-body">
				<?php if(!$_GET['menu']){echo "<div class='alert alert-info'>Bitte wählen Sie eine Kategorie aus der oberen Navigationsbar aus.</div>";} ?>
				<?php require("inc/settings/autors.php"); ?>
				<?php require("inc/settings/formats.php"); ?>
				<?php require("inc/settings/publisher.php"); ?>
				<?php require("inc/settings/genres.php"); ?>
				<?php require("inc/settings/general-settings.php"); ?>
			</div>
		</div>
	</div>
</div>