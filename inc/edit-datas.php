<h3>Daten editieren</h3><hr/>

<?php
    if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich editiert.</div>";}
    if($_GET['error'] == "1"){echo "<div class='alert alert-danger'>Dieses Cover gibt es bereits.</div>";}

    if(isset($_POST['submit'])){
        $id = $_GET['id'];
        $autor = $_POST['autor'];
        $title = $_POST['title'];
        $originaltitle = $_POST['originaltitle'];
        $format = $_POST['format'];
        $isbn = $_POST['isbn'];
        $publisher = $_POST['publisher'];
        $year = $_POST['year'];
        $genre = $_POST['genre'];
        $auflage = $_POST['auflage'];
        $own_data1 = $_POST['own_data1'];
        $own_data2 = $_POST['own_data2'];
        $status = $_POST['status'];

        if(strlen($_FILES['file']['name'])>0){
            $cover = $_FILES['file']['name'];
            $cover_search = array('ü', 'ö', 'ä', 'ß');
            $cover_replace = array('ue', 'oe', 'ae', 'ss');
            $cover = str_replace($cover_search, $cover_replace, $cover);
            move_uploaded_file($_FILES['file']['tmp_name'], "img/cover/".$cover);
        }else{
            $cover = $_POST['cover'];
        }

        mysqli_query($db, "UPDATE buecher
                              SET cover = '".mysqli_real_escape_string($db, $cover)."',
                                  autor = '".mysqli_real_escape_string($db, $autor)."',
                                  title = '".mysqli_real_escape_string($db, $title)."',
                                  originaltitle = '".mysqli_real_escape_string($db, $originaltitle)."',
                                  format = '".mysqli_real_escape_string($db, $format)."',
                                  isbn = '".mysqli_real_escape_string($db, $isbn)."',
                                  publisher = '".mysqli_real_escape_string($db, $publisher)."',
                                  year = '".mysqli_real_escape_string($db, $year)."',
                                  genre = '".mysqli_real_escape_string($db, $genre)."',
                                  auflage = '".mysqli_real_escape_string($db, $auflage)."',
                                  own_data1 = '".mysqli_real_escape_string($db, $own_data1)."',
                                  own_data2 = '".mysqli_real_escape_string($db, $own_data2)."',
                                  status = '".mysqli_real_escape_string($db, $status)."'
                            WHERE id = '".mysqli_real_escape_string($db, $_GET['id'])."'");

        if(mysql_error()){exit(mysql_error());}
        header("Location:?site=edit-datas&id=$id&success=1");
    }

    $sql = mysqli_query($db, "SELECT * FROM buecher WHERE id = '".mysqli_real_escape_string($db, $_GET['id'])."'");
    $row = mysqli_fetch_object($sql);
?>

<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <small><b>Autor</b></small>
                        <select class="form-control" name="autor">
                            <?php
                                echo "<option value='".$row->autor."' selected>".$row->autor."</option>";
                                echo "<option>&ndash;&ndash;&ndash;</option>";
                                $sql0 = mysqli_query($db, "SELECT * FROM autors ORDER BY autor ASC");
                                while($row0 = mysqli_fetch_assoc($sql0)){
                                    echo "<option value='".$row0['autor']."'>".$row0['autor']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Titel</b></small>
                        <input type="text" class="form-control" name="title" placeholder="Titel" <?php echo "value='".$row->title."'"; ?>>
                    </div>
                    <div class="col-md-6">
                        <small><b>Originaltitel</b></small>
                        <input type="text" class="form-control" name="originaltitle" placeholder="Originaltitel" <?php echo "value='".$row->originaltitle."'"; ?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <small><b>Format</b></small>
                        <select class="form-control" name="format">
                            <?php
                                echo "<option value='".$row->format."' selected>".$row->format."</option>";
                                echo "<option>&ndash;&ndash;&ndash;</option>";
                                $sql1 = mysqli_query($db, "SELECT * FROM formats ORDER BY format ASC");
                                while($row1 = mysqli_fetch_assoc($sql1)){
                                    echo "<option value='".$row1['format']."'>".$row1['format']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <small><b>ISBN</b></small>
                        <input type="text" class="form-control" name="isbn" placeholder="ISBN" <?php echo "value='".$row->isbn."'"; ?>>
                    </div>
                    <div class="col-md-3">
                        <small><b>Verlag</b></small>
                        <select class="form-control" name="publisher">
                            <?php
                                echo "<option value='".$row->publisher."' selected>".$row->publisher."</option>";
                                echo "<option>&ndash;&ndash;&ndash;</option>";
                                $sql2 = mysqli_query($db, "SELECT * FROM publishers ORDER BY publisher ASC");
                                while($row2 = mysqli_fetch_assoc($sql2)){
                                    echo "<option value='".$row2['publisher']."'>".$row2['publisher']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <small><b>Jahr</b></small>
                        <input type="text" class="form-control" name="year" placeholder="Jahr" <?php echo "value='".$row->year."'"; ?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Genre</b></small>
                        <select class="form-control" name="genre">
                            <?php
                                echo "<option value='".$row->genre."' selected>".$row->genre."</option>";
                                echo "<option>&ndash;&ndash;&ndash;</option>";
                                $sql3 = mysqli_query($db, "SELECT * FROM genres ORDER BY genre ASC");
                                while($row3 = mysqli_fetch_assoc($sql3)){
                                    echo "<option value='".$row3['genre']."'>".$row3['genre']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <small><b>Auflage</b></small>
                        <input type="text" class="form-control" name="auflage" placeholder="Auflage" <?php echo "value='".$row->auflage."'"; ?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Eigene Daten 1</b></small>
                        <input type="text" class="form-control" name="own_data1" placeholder="Eigene Daten 1" <?php echo "value='".$row->own_data1."'"; ?>>
                    </div>
                    <div class="col-md-6">
                        <small><b>Eigene Daten 2</b></small>
                        <input type="text" class="form-control" name="own_data2" placeholder="Eigene Daten 2" <?php echo "value='".$row->own_data2."'"; ?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Cover</b></small>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" name="file"><br>
                                <input type="text" name="cover" class="form-control" <?php echo "value='".$row->cover."'"; ?> readonly>
                            </div>
                            <div class="col-md-4">
                                <?php echo "<img src='img/cover/".$row->cover."' class='img-responsive img-thumbnail'>"; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <small><b>Status</b></small>
                        <select class="form-control" name="status">
                            <?php
                            echo "<option value='".$row->status."' selected>".$row->status."</option>";
                            echo "<option>&ndash;&ndash;&ndash;</option>"; ?>
                            <option value="gelesen">gelesen</option>
                            <option value="nicht gelesen">nicht gelesen</option>
                            <option value="teilweise gelesen">teilweise gelesen</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block btn-primary" name="submit">Speichern</button>
            </div>
        </div>
    </div>
</form>