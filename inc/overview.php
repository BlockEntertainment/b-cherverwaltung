<div class="col-md-12">
    <div class="panel panel-default panel-bg-1">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 text-right">
                    <?php
                        $sql_title_nums = mysqli_query($db, "SELECT * FROM buecher");
                        $row_num_rows = mysqli_num_rows($sql_title_nums);
                    ?>
                    <b><?php echo number_format($row_num_rows) ?></b> Titel gefunden
                </div>
            </div>
        </div>
    </div>
    <?php if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich gelöscht.</div>";} ?>
    <?php
        $sql = mysqli_query($db, "SELECT * FROM general_settings");
        $row = mysqli_fetch_object($sql);
        $view_listing = $row->view_listing;

        
        if(isset($_POST['sub_search'])){
            if($view_listing == "1"){
                $q = $_POST['q'];
                $sql_search1 = mysqli_query($db, "SELECT * FROM buecher WHERE title LIKE '%".$q."%' OR autor LIKE '%".$q."%' ORDER BY id DESC");
                while($row = mysqli_fetch_assoc($sql_search1)){ ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <?php echo "<a href='?site=edit-datas&id=".$row['id']."'>"; ?><?php echo "<img src='img/cover/".htmlentities($row['cover'])."' alt='".$row['title']."'>"; ?></a>
                            <div class="caption">
                                <h3>
                                    <center>
                                        <?php echo $row['autor'] ?><br>
                                        <span><?php echo $row['title'] ?></span>
                                    </center><br>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Format:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['format'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Jahr:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['year'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Genre:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['genre'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Verlag:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['publisher'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Status:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['status'] ?></span></div>
                                    </div>
                                </h3>
                            </div>
                        </div>
                    </div>
            <?php }
            }

            if($view_listing == "2"){
                echo "
                    <p id='msg'>&nbsp;</p>
                    <table class='table table-striped'>
                        <thead>
                            <tr>
                                <th></th>
                                <th data-sort='string'>Autor</th>
                                <th data-sort='string'>Titel</th>
                                <th data-sort='string'>Format</th>
                                <th>Jahr</th>
                                <th data-sort='string'>Genre</th>
                                <th data-sort='string'>Status</th>
                                <th data-sort='string'>Verlag</th>
                            </tr>
                        </thead>
                        <tbody>
                ";
                $q = $_POST['q'];
                $sql_search2 = mysqli_query($db, "SELECT * FROM buecher WHERE title LIKE '%".$q."%' OR autor LIKE '%".$q."%' ORDER BY id DESC");
                while($row = mysqli_fetch_assoc($sql_search2)){ 
                    echo "<tr id='delbtn_".$row['id']."'>"; ?>
                        <td width="100px">
                            <?php echo "<button class='btn btn-sm btn-danger' onclick='delbook(".$row['id'].");$(\"#delbtn_".$row['id']."\").remove();'>"; ?>
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                            
                            <?php echo "<a href='?site=edit-datas&id=".$row['id']."' class='btn btn-sm btn-warning'>"; ?>
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>
                        <td><?php echo $row['autor'] ?></td>
                        <td><?php echo $row['title'] ?></td>
                        <td><?php echo $row['format'] ?></td>
                        <td><?php echo $row['year'] ?></td>
                        <td><?php echo $row['genre'] ?></td>
                        <td><?php echo $row['status'] ?></td>
                        <td><?php echo $row['publisher'] ?></td>
                    </tr>
            <?php }
                    echo "
                        </tbody>
                    </table>
                    ";
            }
        }
        

        if(!isset($_POST['sub_search'])){
            if($view_listing == "1"){
            echo "<div class='row'>";
                $sql1 = mysqli_query($db, "SELECT * FROM buecher ORDER BY id DESC");
                while($row = mysqli_fetch_assoc($sql1)){ ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                            <?php echo "<a href='?site=edit-datas&id=".$row['id']."'>"; ?><?php echo "<img src='img/cover/".htmlentities($row['cover'])."' alt='".$row['title']."'>"; ?></a>
                            <div class="caption">
                                <h3>
                                    <center>
                                        <?php echo $row['autor'] ?><br>
                                        <span><?php echo $row['title'] ?></span>
                                    </center><br>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Format:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['format'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Jahr:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['year'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Genre:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['genre'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Verlag:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['publisher'] ?></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 text-right"><span><b>Status:</b></span></div>
                                        <div class="col-md-6 text-left"><span><?php echo $row['status'] ?></span></div>
                                    </div>
                                </h3>
                            </div>
                        </div>
                    </div>
            <?php }
            echo "</div>";
            }


            if($view_listing == "2"){
                echo "
                    <p id='msg'>&nbsp;</p>
                    <table class='table table-striped'>
                        <thead>
                            <tr>
                                <th></th>
                                <th data-sort='string'>Autor</th>
                                <th data-sort='string'>Titel</th>
                                <th data-sort='string'>Format</th>
                                <th>Jahr</th>
                                <th data-sort='string'>Genre</th>
                                <th data-sort='string'>Status</th>
                                <th data-sort='string'>Verlag</th>
                            </tr>
                        </thead>
                        <tbody>
                ";
                $sql2 = mysqli_query($db, "SELECT * FROM buecher ORDER BY id DESC");
                while($row = mysqli_fetch_assoc($sql2)){ 
                    echo "<tr id='delbtn_".$row['id']."'>"; ?>
                        <td width="100px">
                            <?php echo "<button class='btn btn-sm btn-danger' onclick='delbook(".$row['id'].");$(\"#delbtn_".$row['id']."\").remove();'>"; ?>
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                            
                            <?php echo "<a href='?site=edit-datas&id=".$row['id']."' class='btn btn-sm btn-warning'>"; ?>
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>
                        <td><?php echo $row['autor'] ?></td>
                        <td><?php echo $row['title'] ?></td>
                        <td><?php echo $row['format'] ?></td>
                        <td><?php echo $row['year'] ?></td>
                        <td><?php echo $row['genre'] ?></td>
                        <td><?php echo $row['status'] ?></td>
                        <td><?php echo $row['publisher'] ?></td>
                    </tr>
            <?php }
                    echo "
                        </tbody>
                    </table>
                    ";
            }
        }
    ?>
    </div>
</div>