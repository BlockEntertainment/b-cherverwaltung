<!-- ADD FORMAT [start] -->
<?php
    if($_GET['menu'] == 'add-format'){
        echo "<h3>Format hinzufügen</h3><hr/>";
        if(isset($_POST['sub_add-format'])){
            $format = $_POST['format'];
                mysqli_query($db, "INSERT INTO formats (format) VALUES ('".mysqli_real_escape_string($db, $format)."')");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=add-format&success=1");
        }

        if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich hinzugefügt.</div>";}
?>

<form method="post">
    <input type="text" name="format" class="form-control" required placeholder="Format"><br>
    <input type="submit" name="sub_add-format" class="btn btn-primary" value="Hinzufügen">
</form>
<?php } ?>
<!-- ADD FORMAT [end] -->


<!-- EDIT FORMAT [start] -->
<?php
    if($_GET['menu'] == 'edit-format'){
        echo "<h3>Format editieren</h3><hr/>";
        $sql = mysqli_query($db, "SELECT * FROM formats ORDER BY format ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=edit-format2&id=".$row['id']."' class='btn btn-sm btn-primary'>".$row['format']."</a>&ensp;";
        }
    }

    if($_GET['menu'] == 'edit-format2'){
        echo "<h3>Format editieren</h3><hr/>";
        if($_GET['success'] == "2"){echo "<div class='alert alert-success'>Erfolgreich editiert.</div>";}

        $id = $_GET['id'];
        $sql = mysqli_query($db, "SELECT * FROM formats WHERE id = '".mysqli_real_escape_string($db, $id)."'");
        $row = mysqli_fetch_object($sql);
        
        if(isset($_POST['sub_edit-format'])){
            $format = $_POST['format'];
                mysqli_query($db, "UPDATE formats SET format = '".mysqli_real_escape_string($db, $format)."' WHERE id = '".mysqli_real_escape_string($db, $id)."'");
                if(mysql_error()){exit(mysql_error());}

                mysqli_query($db, "UPDATE buecher SET format = '".mysqli_real_escape_string($db, $format)."' WHERE format = '".$row->format."'");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=edit-format&success=2");
        }
?>

<form method="post">
    <input type="text" name="format" class="form-control" required placeholder="Format" <?php echo "value='".$row->format."'"; ?>><br>
    <input type="submit" name="sub_edit-format" class="btn btn-primary" value="Editieren">
</form>
<?php } ?>
<!-- EDIT FORMAT [end] -->


<!-- DELETE FORMAT [start] -->
<?php
    if($_GET['menu'] == 'del-format'){
        echo "<h3>Format löschen</h3><hr/>";
        if($_GET['success'] == "3"){echo "<div class='alert alert-success'>Erfolgreich gelöscht.</div>";}
        
        $sql = mysqli_query($db, "SELECT * FROM formats ORDER BY format ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=del-format2&id=".$row['id']."' class='btn btn-sm btn-danger'>".$row['format']."</a>&ensp;";
        }
    }

    if($_GET['menu'] == 'del-format2'){
        echo "<h3>Format löschen</h3><hr/>";
        $id = $_GET['id'];
            mysqli_query($db, "DELETE FROM formats WHERE id = '".mysqli_real_escape_string($db, $id)."'");
            if(mysql_error()){exit(mysql_error());}
            header("Location:?site=settings&menu=del-format&success=3");
    }
?>
<!-- DELETE FORMAT [end] -->