<!-- ADD PUBLISHER [start] -->
<?php
    if($_GET['menu'] == 'add-publisher'){
        echo "<h3>Verlag hinzufügen</h3><hr/>";
        if(isset($_POST['sub_add-publisher'])){
            $publisher = $_POST['publisher'];
                mysqli_query($db, "INSERT INTO publishers (publisher) VALUES ('".mysqli_real_escape_string($db, $publisher)."')");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=add-publisher&success=1");
        }

        if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich hinzugefügt.</div>";}
?>

<form method="post">
    <input type="text" name="publisher" class="form-control" required placeholder="Verlag"><br>
    <input type="submit" name="sub_add-publisher" class="btn btn-primary" value="Hinzufügen">
</form>
<?php } ?>
<!-- ADD PUBLISHER [end] -->


<!-- EDIT PUBLISHER [start] -->
<?php
    if($_GET['menu'] == 'edit-publisher'){
        echo "<h3>Verlag editieren</h3><hr/>";
        if($_GET['success'] == "2"){echo "<div class='alert alert-success'>Erfolgreich editiert.</div>";}

        $sql = mysqli_query($db, "SELECT * FROM publishers ORDER BY publisher ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=edit-publisher2&id=".$row['id']."' class='btn btn-sm btn-primary'>".$row['publisher']."</a>&ensp;";
        }
    }

    if($_GET['menu'] == 'edit-publisher2'){
        echo "<h3>Verlag editieren</h3><hr/>";
        $id = $_GET['id'];
        $sql = mysqli_query($db, "SELECT * FROM publishers WHERE id = '".mysqli_real_escape_string($db, $id)."'");
        $row = mysqli_fetch_object($sql);
        
        if(isset($_POST['sub_edit-publisher'])){
            $publisher = $_POST['publisher'];
                mysqli_query($db, "UPDATE publishers SET publisher = '".mysqli_real_escape_string($db, $publisher)."' WHERE id = '".mysqli_real_escape_string($db, $id)."'");
                if(mysql_error()){exit(mysql_error());}

                mysqli_query($db, "UPDATE buecher SET publisher = '".mysqli_real_escape_string($db, $publisher)."' WHERE publisher = '".$row->publisher."'");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=edit-publisher&success=2");
        }
?>

<form method="post">
    <input type="text" name="publisher" class="form-control" required placeholder="Verlag" <?php echo "value='".$row->publisher."'"; ?>><br>
    <input type="submit" name="sub_edit-publisher" class="btn btn-primary" value="Editieren">
</form>
<?php } ?>
<!-- EDIT PUBLISHER [end] -->


<!-- DELETE PUBLISHER [start] -->
<?php
    if($_GET['menu'] == 'del-publisher'){
        echo "<h3>Verlag löschen</h3><hr/>";
        if($_GET['success'] == "3"){echo "<div class='alert alert-success'>Erfolgreich gelöscht.</div>";}
        
        $sql = mysqli_query($db, "SELECT * FROM publishers ORDER BY publisher ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=del-publisher2&id=".$row['id']."' class='btn btn-sm btn-danger'>".$row['publisher']."</a>&ensp;";
        }
    }

    if($_GET['menu'] == 'del-publisher2'){
        $id = $_GET['id'];
            mysqli_query($db, "DELETE FROM publishers WHERE id = '".mysqli_real_escape_string($db, $id)."'");
            if(mysql_error()){exit(mysql_error());}
            header("Location:?site=settings&menu=del-publisher&success=3");
    }
?>
<!-- DELETE PUBLISHER [end] -->