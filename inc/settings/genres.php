<!-- ADD GENRE [start] -->
<?php
    if($_GET['menu'] == 'add-genre'){
        echo "<h3>Genre hinzufügen</h3><hr/>";
        if(isset($_POST['sub_add-genre'])){
            $genre = $_POST['genre'];
                mysqli_query($db, "INSERT INTO genres (genre) VALUES ('".mysqli_real_escape_string($db, $genre)."')");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=add-genre&success=1");
        }

        if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich hinzugefügt.</div>";}
?>

<form method="post">
    <input type="text" name="genre" class="form-control" required placeholder="Genre"><br>
    <input type="submit" name="sub_add-genre" class="btn btn-primary" value="Hinzufügen">
</form>
<?php } ?>
<!-- ADD GENRE [end] -->


<!-- EDIT GENRE [start] -->
<?php
    if($_GET['menu'] == 'edit-genre'){
        echo "<h3>Genre editieren</h3><hr/>";
        if($_GET['success'] == "2"){echo "<div class='alert alert-success'>Erfolgreich editiert.</div>";}

        $sql = mysqli_query($db, "SELECT * FROM genres ORDER BY genre ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=edit-genre2&id=".$row['id']."' class='btn btn-sm btn-primary'>".$row['genre']."</a>&ensp;";
        }
    }

    if($_GET['menu'] == 'edit-genre2'){
        echo "<h3>Genre editieren</h3><hr/>";
        $id = $_GET['id'];
        $sql = mysqli_query($db, "SELECT * FROM genres WHERE id = '".mysqli_real_escape_string($db, $id)."'");
        $row = mysqli_fetch_object($sql);
        
        if(isset($_POST['sub_edit-genre'])){
            $genre = $_POST['genre'];
                mysqli_query($db, "UPDATE genres SET genre = '".mysqli_real_escape_string($db, $genre)."' WHERE id = '".mysqli_real_escape_string($db, $id)."'");
                if(mysql_error()){exit(mysql_error());}

                mysqli_query($db, "UPDATE buecher SET genre = '".mysqli_real_escape_string($db, $genre)."' WHERE genre = '".$row->genre."'");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=edit-genre&success=2");
        }
?>

<form method="post">
    <input type="text" name="genre" class="form-control" required placeholder="Genre" <?php echo "value='".$row->genre."'"; ?>><br>
    <input type="submit" name="sub_edit-genre" class="btn btn-primary" value="Editieren">
</form>
<?php } ?>
<!-- EDIT GENRE [end] -->


<!-- DELETE GENRE [start] -->
<?php
    if($_GET['menu'] == 'del-genre'){
        echo "<h3>Genre löschen</h3><hr/>";
        if($_GET['success'] == "3"){echo "<div class='alert alert-success'>Erfolgreich gelöscht.</div>";}
        
        $sql = mysqli_query($db, "SELECT * FROM genres ORDER BY genre ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=del-genre2&id=".$row['id']."' class='btn btn-sm btn-danger'>".$row['genre']."</a>&ensp;";
        }
    }

    if($_GET['menu'] == 'del-genre2'){
        $id = $_GET['id'];
            mysqli_query($db, "DELETE FROM genres WHERE id = '".mysqli_real_escape_string($db, $id)."'");
            if(mysql_error()){exit(mysql_error());}
            header("Location:?site=settings&menu=del-genre&success=3");
    }
?>
<!-- DELETE GENRE [end] -->