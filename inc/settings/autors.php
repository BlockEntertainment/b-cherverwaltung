<!-- ADD PUBLISHER [start] -->
<?php
    if($_GET['menu'] == 'add-autor'){
        echo "<h3>Autor hinzufügen</h3><hr/>";
        if(isset($_POST['sub_add-autor'])){
            $autor = $_POST['autor'];
                mysqli_query($db, "INSERT INTO autors (autor) VALUES ('".mysqli_real_escape_string($db, $autor)."')");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=add-autor&success=1");
        }

        if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich hinzugefügt.</div>";}
?>

<form method="post">
    <input type="text" name="autor" class="form-control" required placeholder="Autor"><br>
    <input type="submit" name="sub_add-autor" class="btn btn-primary" value="Hinzufügen">
</form>
<?php } ?>
<!-- ADD PUBLISHER [end] -->


<!-- EDIT PUBLISHER [start] -->
<?php
    if($_GET['menu'] == 'edit-autor'){
        echo "<h3>Autor editieren</h3><hr/>";
        if($_GET['success'] == "2"){echo "<div class='alert alert-success'>Erfolgreich editiert.</div>";}

        $sql = mysqli_query($db, "SELECT * FROM autors ORDER BY autor ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=edit-autor2&id=".$row['id']."' class='btn btn-sm btn-primary'>".$row['autor']."</a><br><div style='margin-bottom:5px;'></div>";
        }
    }

    if($_GET['menu'] == 'edit-autor2'){
        echo "<h3>Autor editieren</h3><hr/>";
        $id = $_GET['id'];
        $sql = mysqli_query($db, "SELECT * FROM autors WHERE id = '".mysqli_real_escape_string($db, $id)."'");
        $row = mysqli_fetch_object($sql);
        
        if(isset($_POST['sub_edit-autor'])){
            $autor = $_POST['autor'];
                mysqli_query($db, "UPDATE autors SET autor = '".mysqli_real_escape_string($db, $autor)."' WHERE id = '".mysqli_real_escape_string($db, $id)."'");
                if(mysql_error()){exit(mysql_error());}

                mysqli_query($db, "UPDATE buecher SET autor = '".mysqli_real_escape_string($db, $autor)."' WHERE autor = '".$row->autor."'");
                if(mysql_error()){exit(mysql_error());}
                header("Location:?site=settings&menu=edit-autor&success=2");
        }
?>

<form method="post">
    <input type="text" name="autor" class="form-control" required placeholder="Autor" <?php echo "value='".$row->autor."'"; ?>><br>
    <input type="submit" name="sub_edit-autor" class="btn btn-primary" value="Editieren">
</form>
<?php } ?>
<!-- EDIT PUBLISHER [end] -->


<!-- DELETE PUBLISHER [start] -->
<?php
    if($_GET['menu'] == 'del-autor'){
        echo "<h3>Autor löschen</h3><hr/>";
        if($_GET['success'] == "3"){echo "<div class='alert alert-success'>Erfolgreich gelöscht.</div>";}

        $sql = mysqli_query($db, "SELECT * FROM autors ORDER BY autor ASC");
        while($row = mysqli_fetch_assoc($sql)){
            echo "<a href='?site=settings&menu=del-autor2&id=".$row['id']."' class='btn btn-sm btn-danger'>".$row['autor']."</a><br><div style='margin-bottom:5px;'></div>";
        }
    }

    if($_GET['menu'] == 'del-autor2'){
        $id = $_GET['id'];
            mysqli_query($db, "DELETE FROM autors WHERE id = '".mysqli_real_escape_string($db, $id)."'");
            if(mysql_error()){exit(mysql_error());}
            header("Location:?site=settings&menu=del-autor&success=3");
    }
?>
<!-- DELETE PUBLISHER [end] -->