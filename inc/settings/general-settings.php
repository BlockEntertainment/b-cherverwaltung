<!-- CHANGE VIEW-LISTING [start] -->
<?php
    if($_GET['menu'] == 'view_listing'){
        echo "<h3>Übersichtsansicht ändern</h3><hr/>";

        
        if(isset($_POST['sub_view_listing_1'])){
            mysqli_query($db, "UPDATE general_settings SET view_listing = '1'");
            if(mysql_error()){exit(mysql_error());}
            header("Location:?site=settings&menu=view_listing&success=1");
        }

        if(isset($_POST['sub_view_listing_2'])){
            mysqli_query($db, "UPDATE general_settings SET view_listing = '2'");
            if(mysql_error()){exit(mysql_error());}
            header("Location:?site=settings&menu=view_listing&success=1");
        }


        $sql = mysqli_query($db, "SELECT * FROM general_settings");
        $row = mysqli_fetch_object($sql);
        if($row->view_listing == "1"){$class_listing1 = "class='btn btn-lg btn-block btn-success'";}else{$class_listing1 = "class='btn btn-lg btn-block btn-danger'";}
        if($row->view_listing == "2"){$class_listing2 = "class='btn btn-lg btn-block btn-success'";}else{$class_listing2 = "class='btn btn-lg btn-block btn-danger'";}

        if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich aktualisiert.</div>";}
?>

<form method="post">
    <div class="row">
        <div class="col-md-3">
            <button type="submit" name="sub_view_listing_1" <?php echo $class_listing1 ?>><span class="glyphicon glyphicon-th"></span> Block-Ansicht</button>
            <button type="submit" name="sub_view_listing_2" <?php echo $class_listing2 ?>><span class="glyphicon glyphicon-align-justify"></span> Zeilen-Ansicht</button>
        </div>
    </div>
</form>

<?php } ?>
<!-- CHANGE VIEW-LISTING [end] -->