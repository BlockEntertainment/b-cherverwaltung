<h3>Daten eingeben</h3><hr/>

<?php
    if($_GET['success'] == "1"){echo "<div class='alert alert-success'>Erfolgreich hinzugefügt.</div>";}

    if(isset($_POST['submit'])){
        $autor = $_POST['autor'];
        $title = $_POST['title'];
        $originaltitle = $_POST['originaltitle'];
        $format = $_POST['format'];
        $isbn = $_POST['isbn'];
        $publisher = $_POST['publisher'];
        $year = $_POST['year'];
        $genre = $_POST['genre'];
        $auflage = $_POST['auflage'];
        $own_data1 = $_POST['own_data1'];
        $own_data2 = $_POST['own_data2'];
        $status = $_POST['status'];

        if(strlen($_FILES['file']['name'])>0){
            $cover = $_FILES['file']['name'];
            $cover_search = array('ü', 'ö', 'ä', 'ß');
            $cover_replace = array('ue', 'oe', 'ae', 'ss');
            $cover = str_replace($cover_search, $cover_replace, $cover);
            move_uploaded_file($_FILES['file']['tmp_name'], "img/cover/".$cover);
        }else{
            $cover = "no-cover.png";
        }

        mysqli_query($db, "INSERT INTO buecher (cover, 
                                                autor, 
                                                title, 
                                                originaltitle,
                                                format,
                                                isbn,
                                                publisher,
                                                year,
                                                genre,
                                                auflage,
                                                own_data1,
                                                own_data2,
                                                status) 
                                VALUES ('".mysqli_real_escape_string($db, $cover)."',
                                        '".mysqli_real_escape_string($db, $autor)."',
                                        '".mysqli_real_escape_string($db, $title)."',
                                        '".mysqli_real_escape_string($db, $originaltitle)."',
                                        '".mysqli_real_escape_string($db, $format)."',
                                        '".mysqli_real_escape_string($db, $isbn)."',
                                        '".mysqli_real_escape_string($db, $publisher)."',
                                        '".mysqli_real_escape_string($db, $year)."',
                                        '".mysqli_real_escape_string($db, $genre)."',
                                        '".mysqli_real_escape_string($db, $auflage)."',
                                        '".mysqli_real_escape_string($db, $own_data1)."',
                                        '".mysqli_real_escape_string($db, $own_data2)."',
                                        '".mysqli_real_escape_string($db, $status)."')");
        if(mysql_error()){exit(mysql_error());}
        header("Location:?site=add-datas&success=1");
    }
?>

<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <small><b>Autor</b></small>
                        <select class="form-control" name="autor">
                            <option>Autor</option>
                            <?php
                                $sql0 = mysqli_query($db, "SELECT * FROM autors ORDER BY autor ASC");
                                while($row0 = mysqli_fetch_assoc($sql0)){
                                    echo "<option value='".$row0['autor']."'>".$row0['autor']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Titel</b></small>
                        <input type="text" class="form-control" name="title" placeholder="Titel">
                    </div>
                    <div class="col-md-6">
                        <small><b>Originaltitel</b></small>
                        <input type="text" class="form-control" name="originaltitle" placeholder="Originaltitel">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <small><b>Format</b></small>
                        <select class="form-control" name="format">
                            <option>Format</option>
                            <?php
                                $sql = mysqli_query($db, "SELECT * FROM formats ORDER BY format ASC");
                                while($row = mysqli_fetch_assoc($sql)){
                                    echo "<option value='".$row['format']."'>".$row['format']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <small><b>ISBN</b></small>
                        <input type="text" class="form-control" name="isbn" placeholder="ISBN">
                    </div>
                    <div class="col-md-3">
                        <small><b>Verlag</b></small>
                        <select class="form-control" name="publisher">
                            <option>Verlag</option>
                            <?php
                                $sql2 = mysqli_query($db, "SELECT * FROM publishers ORDER BY publisher ASC");
                                while($row2 = mysqli_fetch_assoc($sql2)){
                                    echo "<option value='".$row2['publisher']."'>".$row2['publisher']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <small><b>Jahr</b></small>
                        <input type="text" class="form-control" name="year" placeholder="Jahr">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Genre</b></small>
                        <select class="form-control" name="genre">
                            <option>Genre</option>
                            <?php
                                $sql3 = mysqli_query($db, "SELECT * FROM genres ORDER BY genre ASC");
                                while($row3 = mysqli_fetch_assoc($sql3)){
                                    echo "<option value='".$row3['genre']."'>".$row3['genre']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <small><b>Auflage</b></small>
                        <input type="text" class="form-control" name="auflage" placeholder="Auflage">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Eigene Daten 1</b></small>
                        <input type="text" class="form-control" name="own_data1" placeholder="Eigene Daten 1">
                    </div>
                    <div class="col-md-6">
                        <small><b>Eigene Daten 2</b></small>
                        <input type="text" class="form-control" name="own_data2" placeholder="Eigene Daten 2">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <small><b>Cover</b></small>
                        <input type="file" name="file">
                    </div>
                    <div class="col-md-6">
                        <small><b>Status</b></small>
                        <select class="form-control" name="status">
                            <option value="gelesen">gelesen</option>
                            <option value="nicht gelesen">nicht gelesen</option>
                            <option value="teilweise gelesen">teilweise gelesen</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block btn-primary" name="submit">Speichern</button>
            </div>
        </div>
    </div>
</form>