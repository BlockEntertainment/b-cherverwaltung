<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?site=overview"><img src="img/logo.png"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <form class="navbar-form navbar-left" method="post" role="search">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Buchtitel Suchen">
                    <span class="input-group-btn">
                        <button type="submit" name="sub_search" class="btn btn-primary" type="button"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>

            <?php
                if(!$_GET['site'] || $_GET['site'] == 'overview'){$li_overview = "<li class='active'>";}else{$li_overview = "<li>";}
                if($_GET['site'] == 'add-datas'){$li_add_datas = "<li class='active'>";}else{$li_add_datas = "<li>";}
                if($_GET['site'] == 'settings'){$li_settings = "<li class='active'>";}else{$li_settings = "<li>";}
            ?>
            <ul class="nav navbar-nav navbar-right">
                <?php echo $li_overview ?><a href="?site=overview">Übersicht</a></li>
                <?php echo $li_add_datas ?><a href="?site=add-datas">Daten eingeben</a></li>
                <?php echo $li_settings ?><a href="?site=settings">Einstellungen</a></li>
            </ul>
        </div>
    </div>
</nav>