# BÜCHERVERWALTUNG #

Diese kostenfreie Software dient dazu nur die eigenen Bücher dort einzutragen und diese zu sammeln.
Das ganze läuft Local (Offline) und nicht mit Internet.

Systeme: **Windows**

## Wie installiere ich die "Bücherverwaltung"? ##
### Installation von "XAMPP" ###
1. Downloade XAMPP (http://downloads.sourceforge.net/project/xampp/XAMPP%20Windows/1.8.3/xampp-win32-1.8.3-4-VC11-installer.exe)
2. Installiere XAMPP auf C:\xampp
3. Nach der Installation starten Sie den "XAMPP Control-Panel"
4. Gehen Sie nun oben Rechts auf den Button "Config" und machen jeweils einen Haken bei "Apache" und "MySQL" klicken nun auf "Save"
5. Nun drücken Sie jeweils auf den "Start" Button von Apache und MySQL. Wenn jeweils bei beiden Modulen das "grüne" Rechteck zu sehen ist, dann war alles erfolgreich.

### Installation von "Bücherverwaltung" ###
1. Downloaden Sie sich die neuste Version von der Bücherverwaltung (https://bitbucket.org/BlockEntertainment/b-cherverwaltung/downloads)
2. Entpacken Sie den eben gedownloadeten Ordner und fügen den "buecherverwaltung"'s Ordner nach C:\xampp\htdocs
3. Nach dem Sie alles getan haben, können Sie die Bücherverwaltung über das "XAMPP Control-Panel" und dann per "Apache - Admin" Button öffnen oder direkt über den Link (http://localhost/buecherverwaltung/)

## Problembehebungen ##
### Mein Apache Rechteck ist Rot, was nun? ###
Vermutlich sind deine Ports bereits belegt.

Hast du Skype bei dir auf dem Rechner laufen? Wenn ja, dann gehe doch einmal nach **Skype > Aktionen > Optionen > Erweitert > Verbindung** und entferne das häkchen für die Ports 80 und 443.

Starte jetzt Skype neu und probiere noch einmal dein Apache zu starten.